# eda

This project contains a simple template for structuring exploratory data analysis as the precursor to a new predictive modeling or advanced analytics project.

## Analysis Components
- Review, clean, and sort data
    1. Review summary statistics
    2. Simple data cleaning and reformatting
    3. Reclassifying features and/or creating dummy features
    4. Drop redundant or unnecessary features
    5. Address NULL values
    6. Address outliers
    7. Optionally drop rows with NULL values
- Visualize
    1. Target-class distribution bias
    2. Correlation matrix
    3. Scatter plots
    4. Pairplot
    5. Histograms
        [ ] addressing skew

## Environmental setup
- Little environmental setup required, most of this was written in python 3.6 and includes the below python libraries:
    ```
    pandas as pd
    numpy as np
    matplotlib.pylab as plt
    seaborn as sns
    scipy
    sklearn
    random
    imblearn
    ```

## Contributing
If you see something say something!

If you see something dumb in the code for this project or if you have recommendations for updates or improvements feel free to contribute. This was mostly just a way of documenting some personal learnings to help me ramp up faster on similar projects in the future, so it's not a perfect production and is certainly a work-in-progress. 

## Authors and acknowledgment
Credit to Terence Shin and Alicia Horsch (TDS articles) and Nathaniel Jermain (ODS articles) for their indirect contribution to my learnings and this project!